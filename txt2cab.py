#!/bin/env python3
# coding=utf-8

import argparse
parser = argparse.ArgumentParser(description="Dieses Tool wandel Textdateien in denen 0 und 1 vorkommen in Binaries um und ignoriert dabei alles andere. Es eignet sich gut um C Quellcode oder das interene Format vom CubeAnimator in .cab (CubeAnimatorBinary) um zuwandeln. Dabei wird überprüft, ob eine ganze Anzahl an Frames erzeugt wurde")
parser.add_argument("txt_file", help="Textdatei aus der die 0 und 1 umgewandelt werden")
parser.add_argument("cab_file", help="Name für die .cab (CubeAnimatorBinary), die geschrieben (oder auch überschrieben) wird")
parser.add_argument("-8", "--8x8x8", action="store_true", dest="eightmode", help="Schalte in den Modus für den 8x8x8 Cube, dabei sind die Frames 512 bits lang")
args = parser.parse_args()

if args.eightmode:
    frame_modulo = 64
else:
    frame_modulo = 24

txt_file = open(args.txt_file,"r")
txt_file_string = txt_file.read()

values_list = []
current_exponent = 7
current_value = 0

for char in txt_file_string:
    if char == "1" or char == "0":
        if char == "1":
            current_value += 2**current_exponent
        current_exponent -= 1
        if current_exponent == -1:
            values_list.append(current_value)
            current_exponent = 7
            current_value = 0

if current_exponent != 7:
    print("Unpassende Anzahl an bits, Ende.")
elif len(values_list) % frame_modulo != 0:
    print("Unpassende Anzahl an bytes, letzter Frame wäre kaputt, Ende")
else:
    cab_file = open(args.cab_file,"wb")
    cab_file.write(bytearray(values_list))
    cab_file.close()
    print("Es sollten {frames} Frames sein".format(frames=int(len(values_list)/frame_modulo)))
    print("{filename} geschrieben, Ende.".format(filename=args.cab_file))


